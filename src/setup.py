import json
import random
import requests

from . import utils

SYMBOL_OPTIONS = [
    {
        "prompt": "Letters only",
        "categories": ["letter"]
    }, {
        "prompt": "Numbers only",
        "categories": ["number"]
    }, {
        "prompt": "Special symbols only",
        "categories": ["special"]
    }, {
        "prompt": "Letters and numbers",
        "categories": ["letter", "number"]
    }, {
        "prompt": "All",
        "categories": ["letter", "number", "special"]
    }
]

def print_symbols_options():
    '''Print an ordered list of symbol types that can be tested.'''
    print("Symbol types that can be tested:")
    for i in range(1, len(SYMBOL_OPTIONS)+1):
        print("[" + str(i) + "] " + SYMBOL_OPTIONS[i-1]["prompt"])

def input_symbol_types():
    '''Obtain desired symbol type from user.
    
    Returns:
    List containing desired symbol types.
    '''
    print_symbols_options()
    utils.print_newlines(1)
    symbol_num = utils.input_int_in_range(1, len(SYMBOL_OPTIONS), 
        ">> ", "Select an option by entering a number between 1 and " + str(len(SYMBOL_OPTIONS)) + ".")
    return SYMBOL_OPTIONS[symbol_num-1]["categories"]

def get_symbols(num):
    '''Generate symbols to be tested.
    
    Keyword arguments:
    num - number of symbols for the test
    
    Returns:
    List of random symbols
    '''
    symbols_type = input_symbol_types()
    result = []

    symbols = list(utils.data.keys())
    i = 0
    while i < num:
        index = random.randrange(0, len(symbols))

        if utils.data[symbols[index]]["category"] in symbols_type:
            result.append(symbols[index])
            i += 1
    
    return result

def get_words(num):
    '''Generate words to be tested.
    
    Keyword arguments:
    num - number of words for the test
    
    Returns:
    List of random words.
    '''
    url = "https://random-word-api.herokuapp.com/word?number=" + str(num)
    response = requests.get(url)
    words = json.loads(response.text)
    return words

def get_sentences(num):
    '''Generate sentences to be tested.
    
    Keyword arguments:
    num - number of sentence for the test
    
    Returns:
    List of random sentences.
    '''
    sentences = []
    url_start = "https://api.dictionaryapi.dev/api/v2/entries/en/"

    while len(sentences) < num:
        word = get_words(1)
        url = url_start + word[0]
        response = requests.get(url)
        entry = json.loads(response.text)

        try:
            # Grab random definition to use as sentence
            i = random.randrange(0, len(entry[0]["meanings"]))
            j = random.randrange(0, len(entry[0]["meanings"][i]["definitions"]))
            sentence = entry[0]["meanings"][i]["definitions"][j]["definition"]
            sentences.append(sentence)
        except KeyError: # Dictionary couldn't find entry for random word
            continue

    return sentences

def hear_symbols_setup(num):
    '''Setup hearing test of symbols.
    
    Keyword arguments:
    num - number of symbols to test
    
    Returns:
    Dictionary with entries "test type" and "questions"
    Value of "test type" is always "hear"
    Value of "questions" is a list of random symbols.
    '''
    utils.print_newlines(2)

    return {
        "test type": "hear",
        "questions": get_symbols(num)
    }

def hear_words_setup(num):
    '''Setup hearing test of words.
    
    Keyword arguments:
    num - number of words to test
    
    Returns:
    Dictionary with entries "test type" and "questions"
    Value of "test type" is always "hear"
    Value of "questions" is a list of random words.
    '''
    return {
        "test type": "hear",
        "questions": get_words(num)
    }

def hear_sentences_setup(num):
    '''Setup hearing test of sentences.
    
    Keyword arguments:
    num - number of sentences to test
    
    Returns:
    Dictionary with entries "test type" and "questions"
    Value of "test type" is always "hear"
    Value of "questions" is a list of random sentences.
    '''
    return {
        "test type": "hear",
        "questions": get_sentences(num)
    }

def enter_symbols_setup(num):
    '''Setup entering test of symbols.
    
    Keyword arguments:
    num - number of symbols to test
    
    Returns:
    Dictionary with entries "test type" and "questions"
    Value of "test type" is always "enter"
    Value of "questions" is a list of random symbols.
    '''
    utils.print_newlines(2)

    return {
        "test type": "enter",
        "questions": get_symbols(num)
    }
    
def enter_words_setup(num):
    '''Setup entering test of words.
    
    Keyword arguments:
    num - number of words to test
    
    Returns:
    Dictionary with entries "test type" and "questions"
    Value of "test type" is always "enter"
    Value of "questions" is a list of random words.
    '''
    return {
        "test type": "enter",
        "questions": get_words(num)
    }

def enter_sentences_setup(num):
    '''Setup entering test of sentences.
    
    Keyword arguments:
    num - number of sentences to test
    
    Returns:
    Dictionary with entries "test type" and "questions"
    Value of "test type" is always "enter"
    Value of "questions" is a list of random sentences.
    '''
    return {
        "test type": "enter",
        "questions": get_sentences(num)
    }

GAME_OPTIONS = [
    { 
        "prompt": "Hear morse code symbols",
        "setup": hear_symbols_setup
    }, {
        "prompt": "Hear morse code words",
        "setup": hear_words_setup
    }, {
        "prompt": "Hear morse code sentences",
        "setup": hear_sentences_setup
    }, {
        "prompt": "Enter morse code symbols",
        "setup": enter_symbols_setup
    }, {
        "prompt": "Enter morse code words",
        "setup": enter_words_setup
    }, {
        "prompt": "Enter morse code sentences",
        "setup": enter_sentences_setup
    }
]

def print_game_options():
    '''Print an ordered list of games.'''
    for i in range(1, len(GAME_OPTIONS)+1):
        print("[" + str(i) + "] " + GAME_OPTIONS[i-1]["prompt"])

def choose_game():
    '''Obtain desired game & game settings from the user.
    
    Returns:
    Dictionary with entries "test type" and "questions"
    Value of "test type" is either "hear" or "enter"
    Value of "questions" is a list of random strings.
    '''
    print_game_options()
    utils.print_newlines(1)
    game_num = utils.input_int_in_range(1, len(GAME_OPTIONS), ">> ", "Select an option by entering a number between 1 and " + str(len(GAME_OPTIONS)) + ".")

    utils.print_newlines(2)
    print("How many questions?\n")
    num_questions = utils.input_pos_int(">> ")

    setup = GAME_OPTIONS[game_num-1]["setup"](num_questions)

    utils.print_newlines(2)
    return setup