import datetime
import pyHook
import pythoncom

from . import settings
from . import utils

def on_keydown_event(event):
    '''Event handler for keydown events.'''
    global result

    if event.Key == "R":
        result = [[""]]
        utils.print_now("\nResetting...\n>> ")

    elif event.Key == "Return":
        global is_finished_input
        is_finished_input = True

    elif event.Key == "Space":
        global is_currently_pressing_key
        if not is_currently_pressing_key:
            is_currently_pressing_key = True
            global last_keydown_time, last_keyup_time
            last_keydown_time = datetime.datetime.now()

            if last_keyup_time != 0:
                td = last_keydown_time - last_keyup_time
                diff = utils.get_total_milliseconds(td)

                if diff >= settings.WORD_GAP_LENGTH:
                    # Long enough wait to signal a new word
                    result.append([""])
                    utils.print_now("\n>> ")
                elif diff >= settings.LETTER_GAP_LENGTH:
                    # Long enough wait to signal a new letter
                    result[-1].append("")
                    utils.print_now(" ")

    return True
            
def on_keyup_event(event):
    '''Event handler for keyup events.'''
    global result

    if event.Key == "Space":
        global is_currently_pressing_key, last_keydown_time, last_keyup_time

        last_keyup_time = datetime.datetime.now()
        td = last_keyup_time - last_keydown_time
        diff = utils.get_total_milliseconds(td)

        if diff < (settings.DOT_LENGTH + settings.DASH_LENGTH) / 2:
            # This is way more forgiving than trying to force user
            # to press spacebar exactly for a certain amount of time
            result[-1][-1] += "."
            utils.print_now(".")
        else:
            result[-1][-1] += "-"
            utils.print_now("-")
        
        is_currently_pressing_key = False
    
    return True

def get_morse_input():
    '''Obtain morse code from standard input.
    
    Returns:
    List of list of strings. Each string is one symbol. 
    Symbols are contained in a list that contains one word.
    Words are contained in a list for a full message.
    '''
    global last_keydown_time, last_keyup_time, is_currently_pressing_key, result, is_finished_input
    last_keydown_time = 0
    last_keyup_time = 0
    is_currently_pressing_key = False
    result = [[""]]
    is_finished_input = False

    # Set up event listeners to keyboard events
    hm = pyHook.HookManager()
    hm.KeyDown = on_keydown_event
    hm.KeyUp = on_keyup_event
    hm.HookKeyboard()

    utils.print_now(">> ")

    while not is_finished_input:
        pythoncom.PumpWaitingMessages()
    
    hm.UnhookKeyboard()
    
    return result