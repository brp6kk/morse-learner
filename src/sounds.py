import os
import threading

# Hides "Welcome to Pygame" message in the console.
os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "hide"

import pygame, pygame.sndarray
import numpy

from . import settings, translate

FREQUENCY = 256
VOLUME = 4096
SAMPLE_RATE = 44100

pygame.mixer.pre_init(SAMPLE_RATE, -16, 1)
pygame.init()

def sine_wave(hz, peak, n_samples):
    '''Compute N samples of a sine wave with given frequency and peak amplitude.'''
    # Most of the GameObjectAudio class was based on Akkana's code.
    # This function was basically copied from them.
    # https://shallowsky.com/blog/programming/python-play-chords.html
    length = n_samples / float(hz)
    omega = numpy.pi * 2 / length
    xvalues = numpy.arange(int(length)) * omega
    onecycle = peak * numpy.sin(xvalues)
    return numpy.resize(onecycle, (n_samples,)).astype(numpy.int16)

def play_sound_for(freq, vol, rate, duration):
    '''Play a sine wave for specified duration.
    
    Keyword arguments:
    freq - Frequency for sine wave.
    vol - Volume for sine wave.
    rate - Length for sine wave.
    duration - Time sine wave should be played (milliseconds).
    '''
    wave = sine_wave(freq, vol, rate)
    buffer = numpy.repeat(wave.reshape(SAMPLE_RATE, 1), 2, axis=1)
    sound = pygame.sndarray.make_sound(buffer)
    sound.play(-1)
    pygame.time.delay(duration)
    sound.stop()

def play_dot():
    '''Play morse code dot'''
    play_sound_for(FREQUENCY, VOLUME, SAMPLE_RATE, settings.DOT_LENGTH)

def play_dash():
    '''Play morse code dash'''
    play_sound_for(FREQUENCY, VOLUME, SAMPLE_RATE, settings.DASH_LENGTH)

def pause_for(duration):
    '''Pause program for specified amount of time.
    
    Keyword arguments:
    duration - Delay time in milliseconds.
    '''
    pygame.time.delay(duration)

def pause_mark():
    '''Pause as appropriate between morse code marks (single dot/dash)'''
    pause_for(settings.MARK_GAP_LENGTH)

def pause_letter():
    '''Pause as appropriate between morse code letters.'''
    pause_for(settings.LETTER_GAP_LENGTH)

def pause_word():
    '''Pause as appropriate between morse code words.'''
    pause_for(settings.WORD_GAP_LENGTH)

def play_morse(morse):
    '''Play given morse code.
    
    Keyword arguments:
    morse - list of list of strings. Each string is one symbol. 
    Symbols are contained in a list that contains one word.
    Words are contained in a list for a full message.
    '''
    pause_for(500)
    for word in morse:
        for symbol in word:
            for mark in symbol:
                if mark == ".":
                    play_dot()
                else: # mark == "-"
                    play_dash()
                pause_mark()
            pause_letter()
        pause_word()

def play_full_question(question):
    '''Play sound corresponding to given question.
    Notes this starts a new thread to play the sound,
    so that the main thread is not blocked.
    
    Keyword arguments:
    question - String in standard non-coded language.
    '''
    morse = translate.translate_standard_to_morse(question)
    morse_thread = threading.Thread(target=play_morse, args=(morse,))
    morse_thread.start()
