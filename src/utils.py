import json

DATA_FILE = "src/data.json"
with open(DATA_FILE, "r") as rfile:
    data = json.load(rfile)

def input_int(prompt="Enter an integer: ", error="Please enter an integer."):
    '''Get an integer value from standard input.
    
    Keyword arguments:
    prompt - Displayed to prompt user to enter an integer.
    error - Displayed when user fails to enter an integer.

    Returns:
    Integer specified by the user.
    '''
    while True:
        num = input(prompt)
        try:
            num = int(num)
            break
        except:
            print(error)

    return num

def input_int_in_range(start, end, prompt=None, error="Please enter an integer in the given range."):
    '''Get an integer value within a range from standard input.
    
    Keyword arguments:
    start - Starting value for range, inclusive.
    end - Ending value for range, inclusive.
    prompt - Displayed to prompt user to enter an integer.
    error - Displayed when user fails to enter an integer in the proper range.

    Returns:
    Integer specified by the user, contained in [start, end]
    '''
    if prompt is None:
        prompt = "Enter an integer between " + str(start) + " and " + str(end) + " (inclusive): "

    while True:
        num = input_int(prompt, error)
        if num < start or num > end:
            print(error)
        else:
            break

    return num

def input_pos_int(prompt="Enter a positive integer: ", error="Please enter a positive integer."):
    '''Get a positive integer from standard input.
    
    Keyword arguments:
    prompt - Displayed to prompt user to enter a positive integer.
    error - Displayed when user fails to enter a positive integer.

    Returns:
    Positive integer specified by the user.
    '''
    while True:
        num = input_int(prompt, error)
        if num <= 0:
            print(error)
        else:
            break
    
    return num

def print_newlines(num=1):
    '''Print specified number of newlines to standard output.
    
    Keyword arguments:
    num - Number of newlines to print
    '''
    for _ in range(0, num):
        print()

def print_now(message):
    '''Force the message to be flushed to standard output.
    
    Keyword arguments:
    message - Message to be printed.
    '''
    print(message, end="", flush=True)

def get_total_milliseconds(td):
    '''Get the total number of microseconds in a timedelta object.
    
    Keyword arguments:
    td - timedelta object
    
    Returns:
    Number of microseconds
    '''
    return (td.total_seconds() * 1000)