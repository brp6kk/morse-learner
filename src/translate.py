from . import utils

def get_morse_dict():
    '''Create dict where keys are morse code & values are starndard characters.'''
    result = dict()
    for key in utils.data:
        result[utils.data[key]["code"]] = key
    return result

morse_dict = get_morse_dict()

def translate_morse_to_standard(morse):
    '''Translate morse code to standard language.
    
    Keyword parameters:
    morse - list of list of strings. Each string is one symbol. 
    Symbols are contained in a list that contains one word.
    Words are contained in a list for a full message.

    Returns:
    String containing translation of the morse code message.
    '''
    morse = clean_morse_code(morse)

    standard = ""
    for word in morse:
        for symbol in word:
            standard += morse_dict[symbol]
        standard += " " # Add space between words
    return standard[:-1] # Remove trailing space

def translate_standard_to_morse(standard):
    '''Translate standard language to morse code.
    
    Keyword parameters:
    standard - String containing message.

    Returns:
    List of list of strings. Each string is one symbol. 
    Symbols are contained in a list that contains one word.
    Words are contained in a list for a full message.
    '''
    morse = []
    for word in standard.split(" "):
        morse_word = []
        word = word.lower() # Morse code is not case sensitive
        for symbol in word:
            if symbol not in utils.data.keys():
                # Some symbols do not exist in morse alphabet.
                raise KeyError
            else:
                morse_word.append(utils.data[symbol]["code"])
        morse.append(morse_word)
                
    return morse

def clean_morse_code(morse):
    '''Remove empty words or symbols from morse code.
    
    Keyword arguments:
    morse - list of list of strings. Each string is one symbol. 
    Symbols are contained in a list that contains one word.
    Words are contained in a list for a full message.

    Returns:
    Same as input, but with empty strings and empty word lists removed.
    '''
    i = 0
    j = 0
    while i < len(morse):
        while j < len(morse[i]):
            if morse[i][j] == "":
                morse[i] = morse[i][:j] + morse[i][j+1:]
            else:
                j += 1
        
        if len(morse[i]) == 0:
            # After removing all spaces, length of word in 0
            morse = morse[:i] + morse[i+1:]
        else:
            i += 1
        j = 0

    return morse