import src.listener as listener
import src.setup as setup
import src.sounds as sounds
import src.translate as translate
import src.utils as utils

def run_hear_test(questions):
    '''Run test in which user listens to morse code.
    
    Keyword arguments:
    questions - List of strings, each string being a non-coded word to test.
    
    Returns:
    Number of questions answered correctly.
    '''
    num_correct = 0

    for q in questions["questions"]:
        print("Listen:")
        
        while True:
            sounds.play_full_question(q)
            answer = input(">> ")
            if q == answer:
                print("Correct!")
                num_correct += 1
            elif answer == "^":
                # Play sound again
                continue
            else:
                print("Incorrect")
                print("Answer: " + q)
            
            break
        
        utils.print_newlines(2)
    
    return num_correct

def run_enter_test(questions):
    '''Run test in which user enters morse code
    
    Keyword arguments:
    questions - List of strings, each string being a non-coded word to test.
    
    Returns:
    Number of questions answered correctly.
    '''
    num_correct = 0

    for q in questions["questions"]:
        print(q)

        answer = listener.get_morse_input()
        
        input() # Force flush of stdin

        try:
            answer = translate.translate_morse_to_standard(answer)
        except KeyError: # Invalid morse code
            answer = ""

        if q == answer:
            print("Correct!")
            num_correct += 1
        else:
            print("Incorrect")
            print("Answer: Listen")
            sounds.play_morse(translate.translate_standard_to_morse(q))
        
        utils.print_newlines(2)
    
    return num_correct

def run_again():
    '''Use standard IO to determine if user wants to play game again.
    
    Returns:
    True - user indicated they wanted to play again.
    False - user indicated they wanted to quit the program.
    '''
    # Prompt
    print("[n] New test")
    print("[q] Quit")

    while True:
        result = input(">> ")
        if result == "q":
            return False
        elif result == "n":
            return True
        else: # Bad input
            print("Select [n] or [q].")

def run():
    '''Main runner for app.'''

    TEST_OPTIONS = [
        {
            "test type": "hear",
            "function": run_hear_test
        }, {
            "test type": "enter",
            "function": run_enter_test
        }
    ]

    print("***** Welcome to MORSE LEARNER *****\n\n")

    # Game loop
    while True:
        # Choose & run game
        questions = setup.choose_game()
        for opt in TEST_OPTIONS:
            if opt["test type"] == questions["test type"]:
                num_correct = opt["function"](questions)
                break
        
        # Final score
        print("Result: " + str(num_correct) + "/" + str(len(questions["questions"])))
        utils.print_newlines(2)

        # Prompt to play again
        if not run_again():
            break
        utils.print_newlines(4)
    
    print("\n\n\n\nGoodbye")

if __name__ == '__main__':
    run()