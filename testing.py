import datetime
import unittest

import src.settings as settings
import src.sounds as sounds
import src.translate as translate
import src.utils as utils

class TestListener(unittest.TestCase):
    '''Test case to test src/listener.py
    Note the following listener functions are not tested,
    as they involve standard IO:
    on_keydown_event
    on_keyup_event
    get_morse_input
    '''
    pass

class TestSetup(unittest.TestCase):
    '''Test case to test src/setup.py
    Note the following setup functions are not tested,
    as they involve standard IO/async calls:
    print_symbols_options
    get_symbol_types
    get_symbols
    get_words
    get_sentences
    hear_symbols_setup
    hear_words_setup
    hear_sentences_setup
    enter_symbols_setup
    enter_words_setup
    enter_sentences_setup
    print_game_options
    choose_game
    '''
    pass

class TestSounds(unittest.TestCase):
    '''Test case to test src/sounds.py
    Note the following sounds functions are not tested,
    as they involve audio:
    sine_wave
    play_sound_for
    play_dot
    play_dash
    play_morse
    play_full_question
    '''

    def pause_helper(self, pause_func, duration):
        '''Checks that a function pauses for a given duration of time.
        
        Keyword arguments:
        pause_func - Function that is supposed to halt execution.
        duration - Length of time in milliseconds that pause_func is supposed to pause.
        '''
        start = datetime.datetime.now()
        pause_func()
        end = datetime.datetime.now()
        delta = (end - start).total_seconds() * 1000
        dif = abs(delta - duration)
        self.assertLess(dif, 5) # Fine if off by 5 ms

    def test_pause_for(self):
        '''Test pause_for'''
        for i in range(0, 5):
            with self.subTest(i=i):
                self.pause_helper(lambda : sounds.pause_for(i*100), i*100)
    
    def test_pause_mark(self):
        '''Test pause_mark'''
        self.pause_helper(sounds.pause_mark, settings.MARK_GAP_LENGTH)
    
    def test_pause_letter(self):
        '''Test pause_letter'''
        self.pause_helper(sounds.pause_letter, settings.LETTER_GAP_LENGTH)
    
    def test_pause_word(self):
        '''Test pause_word'''
        self.pause_helper(sounds.pause_word, settings.WORD_GAP_LENGTH)

class TestTranslate(unittest.TestCase):
    '''Test case to test src/translate.py'''

    def test_translate_morse_to_standard(self):
        '''Test translate_morse_to_standard.'''

        # These translations should succeed.
        self.assertEqual("a", translate.translate_morse_to_standard([[".-"]]))
        self.assertEqual("word", translate.translate_morse_to_standard([[".--", "---", ".-.", "-.."]]))
        self.assertEqual("the quick brown fox jumped over the lazy dog", translate.translate_morse_to_standard([
            ["-", "....", "."], ["--.-", "..-", "..", "-.-.", "-.-"], ["-...", ".-.", "---", ".--", "-."],
            ["..-.", "---", "-..-"], [".---", "..-", "--", ".--.", ".", "-.."], ["---", "...-", ".", ".-."],
            ["-", "....", "."], [".-..", ".-", "--..", "-.--"], ["-..", "---", "--."]
        ]))
        self.assertEqual("5739421680", translate.translate_morse_to_standard([[
            ".....", "--...", "...--", "----.", "....-", "..---", ".----", "-....", "---..", "-----"]]))
        self.assertEqual("statement. but, question? who/(what) & when - unknown!", translate.translate_morse_to_standard([
            ["...", "-", ".-", "-", ".", "--", ".", "-.", "-", ".-.-.-"], ["-...", "..-", "-", "--..--"], 
            ["--.-", "..-", ".", "...", "-", "..", "---", "-.", "..--.."], 
            [".--", "....", "---", "-..-.", "-.--.", ".--", "....", ".-", "-", "-.--.-"], [".-..."],
            [".--", "....", ".", "-."], ["-....-"], ["..-", "-.", "-.-", "-.", "---", ".--", "-.", "-.-.--"]
        ]))
        
        # Translations should fail due to improper morse code input.
        with self.assertRaises(KeyError):
            translate.translate_morse_to_standard([["abc"]])
        with self.assertRaises(KeyError):
            translate.translate_morse_to_standard(["abc"])
        with self.assertRaises(KeyError):
            translate.translate_morse_to_standard("abc")
        
        self.assertEqual("sos", translate.translate_morse_to_standard([["...", "---", "", "..."], [], [""]]))
    
    def test_translate_standard_to_morse(self):
        '''Test translate_standard_to_morse'''

        # These translations should succeed
        self.assertEqual(translate.translate_standard_to_morse("a"), [[".-"]])
        self.assertEqual(translate.translate_standard_to_morse("word"), [[".--", "---", ".-.", "-.."]])
        self.assertEqual(translate.translate_standard_to_morse("the quick brown fox jumped over the lazy dog"), [
            ["-", "....", "."], ["--.-", "..-", "..", "-.-.", "-.-"], ["-...", ".-.", "---", ".--", "-."],
            ["..-.", "---", "-..-"], [".---", "..-", "--", ".--.", ".", "-.."], ["---", "...-", ".", ".-."],
            ["-", "....", "."], [".-..", ".-", "--..", "-.--"], ["-..", "---", "--."]
        ])
        self.assertEqual(translate.translate_standard_to_morse("5739421680"), 
            [[".....", "--...", "...--", "----.", "....-", "..---", ".----", "-....", "---..", "-----"]])
        self.assertEqual(translate.translate_standard_to_morse("statement. but, question? who/(what) & when - unknown!"), [
            ["...", "-", ".-", "-", ".", "--", ".", "-.", "-", ".-.-.-"], ["-...", "..-", "-", "--..--"], 
            ["--.-", "..-", ".", "...", "-", "..", "---", "-.", "..--.."], 
            [".--", "....", "---", "-..-.", "-.--.", ".--", "....", ".-", "-", "-.--.-"], [".-..."],
            [".--", "....", ".", "-."], ["-....-"], ["..-", "-.", "-.-", "-.", "---", ".--", "-.", "-.-.--"]
        ])
        self.assertEqual(translate.translate_standard_to_morse("SOS"), [["...", "---", "..."]])

        # Translation should fail due to them including invalid characters.
        with self.assertRaises(KeyError):
            translate.translate_standard_to_morse("#%^*")
    
    def test_clean_morse_code(self):
        '''Test clean_morse_code'''
        self.assertEqual([], translate.clean_morse_code([[""]]))
        self.assertEqual([["...", "---", "..."]], translate.clean_morse_code([["...", "", "---", "..."]]))
        self.assertEqual([["."]], translate.clean_morse_code([["", ".", ""], [], [""]]))

class TestUtils(unittest.TestCase):
    '''Test case to test src/utils.py
    Note the following utils functions are not tested,
    as they involve standard IO:
    input_int
    input_pos_int
    input_int_in_range
    print_newlines
    print_now
    '''
    
    def test_get_total_milliseconds(self):
        '''Test get_total_milliseconds'''
        self.assertEqual(utils.get_total_milliseconds(datetime.timedelta(seconds=1)), 1000)
        self.assertEqual(utils.get_total_milliseconds(datetime.timedelta(microseconds=1000)), 1)
        self.assertEqual(utils.get_total_milliseconds(
            datetime.timedelta(days=1, seconds=1, microseconds=1, milliseconds=1, minutes=1, hours=1, weeks=1)), 
            694861001.001) # Got this number via a calculator


if __name__ == '__main__':
    unittest.main()