# MORSE LEARNER

CLI Python tool to learn morse code.

**Important: read this document in its entirety! Program does not provide instructions**

**This program only works on Windows machines!**

# Running the app

1. Ensure you have [Python](https://www.python.org/downloads/) 3.7 or later on your machine.
2. `pip install -r requirements.txt`
3. Download [PyHook](https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyhook)
4. `pip install [PyHook filename].whl`
5. `python app.py`

# Instructions

Before beginning a game, you have to set it up.

First, choose the game type:

1. Hear morse code symbol input, answer with standard symbol
2. Hear morse code word input, answer with standard word
3. Hear morse code sentence input, answer with standard sentence
4. Display standard symbol input, answer with morse code symbol
5. Display standard word input, answer with morse code word
6. Display standard sentence input, answer with morse code sentence

Next, enter the number of questions.

If you chose option 1 or 4 (testing on individual symbols), choose the types of symbols you'd like to be tested on:
1. Letters only
2. Numbers only
3. Special symbols only (e.g., !, &)
4. Letters and numbers
5. All

If you chose option 1, 2, or 3 (hearing tests), for each question a series of dots and dashes will play. Enter what you think the corresponding language is. If you want to hear the morse code again, enter [^].

If you chose option 4, 5, or 6 (input tests), for each question a symbol, word, or sentence will appear (depending on exact test type). When you are ready, begin entering morse code with the space bar. When you are done, press [enter]. If you want to start the question over, press [r].

When doing an input test, your input will appear on the screen as dots and dashes. Letters are separated by spaces, and words are separated by lines.

# Notes

* Currently, the only clean way of exiting the program is to enter [q] after completing a game (as prompted).
* During input tests, the input buffer is not cleared until after the user is done entering a question's morse code.
  * If the user entered non-space keys, they will show up on screen.
  * While this is kinda ugly, I'm not sure how to fix it.
  * However, this is so minor that I am not focusing on this for now.

# Resources used
1. ["Play notes, chords and arbitrary waveforms from Python"](https://shallowsky.com/blog/programming/python-play-chords.html)
2. [Morse code information](https://en.wikipedia.org/wiki/Morse_code)
3. [Random words API](https://random-word-api.herokuapp.com/home)
4. [Dictionary API](https://dictionaryapi.dev/)